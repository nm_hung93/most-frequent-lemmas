################
# Basic imports
################

import urllib.request
from pathlib import Path
import os
import zipfile
import natsort

rootdir = Path(".").absolute()
data_dir = os.path.join(rootdir, 'data')

################
# Download data
################
# Peter Norvigs - The 1/3 million most frequent words, all lowercase, with counts.
# source: https://norvig.com/ngrams/
urllib.request.urlretrieve("https://norvig.com/ngrams/count_1w.txt", os.path.join(data_dir,'pnorvig_count_1w.txt'))

# Paul Nations 10000 words list
# source: https://www.wgtn.ac.nz/lals/resources/paul-nations-resources/vocabulary-lists
pnation_dir = os.path.join(data_dir, 'pnation')
Path(pnation_dir).mkdir(parents=True, exist_ok=True)

zipped_file = os.path.join(pnation_dir,'pnation_10000headwords.zip')
urllib.request.urlretrieve("https://www.wgtn.ac.nz/lals/resources/paul-nations-resources/paul-nations-publications/publications/documents/10000-headwords.zip",zipped_file)

with zipfile.ZipFile(zipped_file, 'r') as zip_ref:
    zip_ref.extractall(pnation_dir)

with open(os.path.join(data_dir, 'pnation_1000headwords.txt'), "a") as pnation_txt:
    for headwords in natsort.natsorted(Path(pnation_dir).glob("*.txt"), reverse=False):
       with open(headwords) as part:
           pnation_txt.write(part.read())
